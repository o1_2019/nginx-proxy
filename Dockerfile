FROM openresty/openresty
COPY ./ /etc/nginx/conf.d
COPY ./check_auth_bearer_only.lua /usr/local/openresty/nginx/
RUN ["mkdir", "/etc/nginx/certs"]
RUN /usr/local/openresty/bin/opm install zmartzone/lua-resty-openidc
COPY  certs/ /etc/nginx/certs/