local opts = {
    discovery = "https://192.168.0.4:8443/auth/realms/bethub/.well-known/openid-configuration",
    ssl_verify = "no",
}

local res, err = require("resty.openidc").bearer_jwt_verify(opts)

if err or not res then
   ngx.status = 401
   ngx.say(err and err or "no access_token provided")
   ngx.exit(ngx.HTTP_UNAUTHORIZED)
end